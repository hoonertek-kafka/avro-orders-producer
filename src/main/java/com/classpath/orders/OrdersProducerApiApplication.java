package com.classpath.orders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdersProducerApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrdersProducerApiApplication.class, args);
	}

}
