package com.classpath.orders.client;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import com.classpath.orders.model.Order;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;

@Component
public class KafkaOrderConsumer implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {

		Properties properties = new Properties();
		properties.put(ConsumerConfig.GROUP_ID_CONFIG, "pradeep-orders-consumer-2");
		properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "139.59.62.239:9092");
		properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class);
		properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
		properties.put("schema.registry.url", "http://134.209.157.197:8081");

		KafkaConsumer<Integer, Order> kafkaConsumer = new KafkaConsumer<Integer, Order>(properties);

		kafkaConsumer.subscribe(Arrays.asList("pradeep-orders-topic-v2"));

		System.out.println("Consumer has started ::::");

		boolean flag = true;
		while (flag) {
			System.out.println("Inside the while loop::");
			ConsumerRecords<Integer, Order> records = kafkaConsumer.poll(Duration.of(1000, ChronoUnit.MILLIS));
			Thread.sleep(2000);
			System.out.println("Records:: "+ records.count());
			for (ConsumerRecord<Integer, Order> record : records) {
				System.out.println("************************************");
				System.out.println("Record key :: " + record.key());
				System.out.println("Record Value :: " + record.value());
				System.out.println("************************************");
			}
		}
		kafkaConsumer.close();
	}
}
