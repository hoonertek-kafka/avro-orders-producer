package com.classpath.orders.client;

import java.time.ZoneId;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.classpath.orders.model.Order;
import com.github.javafaker.Faker;

import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroSerializer;


public class KafkaOrderProducer{

	
	public static void main(String args[]) throws Exception {
		Properties producerProperties = new Properties();
		producerProperties.put(ProducerConfig.CLIENT_ID_CONFIG, "orders-producer-app");
		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "139.59.62.239:9092");
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
		producerProperties.put("schema.registry.url", "http://134.209.157.197:8081");
		Faker faker = new Faker();
		String name = faker.name().firstName();
		KafkaProducer<Integer, Order> kafkaProducer = new KafkaProducer(producerProperties);
		
		IntStream.range(0, 2000).forEach(index -> {
			 Order order = Order.newBuilder()
					 .setOrderId(faker.number().randomDigitNotZero())
					 .setName(name)
					 .setEmail(name+"@"+faker.internet().domainName())
					 .setPrice(faker.number().randomDouble(2, 2000, 3000))
					 .setQty(faker.number().randomDigitNotZero())
					 .build();
			ProducerRecord<Integer, Order> record = new ProducerRecord<>("pradeep-orders-topic-v2", index, order);
			kafkaProducer.send(record);
			System.out.println("***********************");
			System.out.println("Producer is sending the record :: "+ record);
			System.out.println("***********************");
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
		kafkaProducer.close();
	}
	
	
	

}
